package lpuccio.TPNote;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestGroupe {
	
	public Groupe g;
	
	@BeforeEach
	public void setUp() {
		g = new Groupe("groupe");
	}
	
	@Test
	void testGroupeIdDeuxGroupes() {
		int id = g.getId() + 1;
		Groupe gg = new Groupe("groupe");
		assertEquals(id, gg.getId());
	}

	@Test
	void testGroupeGetNom() {
		assertEquals("groupe", g.getNom());
	}
	
	@Test
	void testGroupeIsVoeuReadyFalse() {
		assertFalse(g.isVoeuReady());
	}
	
	@Test
	void testGroupeIsVoeuReadyTrue() {
		g.setVoeuFini(true);
		assertTrue(g.isVoeuReady());
	}
	
	@Test
	void testGroupeSetNom() {
		g.setNom("bruce");
		assertEquals("bruce", g.getNom());
	}
	
	@Test
	void testGroupeToString() {
		assertEquals(g.getNom()+" (id:"+g.getId()+")\n", g.toString());
	}
	
	@Test
	void testGroupeToStringUnVoeu() throws VoeuIndisponible {
		Sujet s = new Sujet("suj");
		g.addVoeu(1, s);
		int id = g.getId();
		String nom = g.getNom();
		assertEquals(nom+" (id:"+id+")\n--1: "+s.toString()+"\n", g.toString());
	}
	
	@Test
	void testGroupeAddVoeu() {
		VoeuIndisponible errless = assertThrows(VoeuIndisponible.class, () -> {
			g.addVoeu(0, null);
		});
        assertEquals(errless.getMessage(), "L'ordre du voeu doit être entre 1 et 5");
        VoeuIndisponible errmore = assertThrows(VoeuIndisponible.class, () -> {
			g.addVoeu(6, null);
		});
        assertEquals(errmore.getMessage(), "L'ordre du voeu doit être entre 1 et 5");
	}
	
	@Test
	void testGroupeAjouterVoeuCorrect() throws VoeuIndisponible {
		assertTrue(g.addVoeu(1, null));
	}
	
	@Test
	void testGroupeAjouterVoeuSujetDejaChoisi() throws VoeuIndisponible {
		Sujet s = new Sujet("suj1");
		g.addVoeu(1, s);
		VoeuIndisponible err = assertThrows(VoeuIndisponible.class, () -> {
			g.addVoeu(2, s);
		});
        assertEquals(err.getMessage(), "Le sujet a déjà été choisi");
	}
	
	@Test
	void testGroupeAjouterVoeuOrdreDejaRempli() throws VoeuIndisponible {
		Sujet s = new Sujet("suj1");
		Sujet s2 = new Sujet("suj2");
		assertTrue(g.addVoeu(1, s));
		VoeuIndisponible err = assertThrows(VoeuIndisponible.class, () -> {
			g.addVoeu(1	, s2);
		});
        assertEquals(err.getMessage(), "L'ordre a déjà été rempli");
	}
	
	@Test
	void testGroupeSetVoeu() {
		HashMap<Integer, Sujet> v = new HashMap<Integer, Sujet>();
		Sujet s = new Sujet("suj1");
		v.put(1, s);
		g.setVoeux(v);
		VoeuIndisponible err = assertThrows(VoeuIndisponible.class, () -> {g.addVoeu(2, s);});
        assertEquals(err.getMessage(), "Le sujet a déjà été choisi");
	}
	
	@Test
	void testGroupeConstructorAllInfo() {
		Groupe gg = new Groupe(444, 444, "gg", new HashMap<Integer, Sujet>(), false);
		assertFalse(gg.isVoeuReady());
		assertEquals(gg.getNom(),"gg");
		
	}
	
	@Test
	void testGroupeAddFiveVoeuxAndReady() throws VoeuIndisponible{
		Sujet s1 = new Sujet("suj");
		Sujet s2 = new Sujet("suj");
		Sujet s3 = new Sujet("suj");
		Sujet s4 = new Sujet("suj");
		Sujet s5 = new Sujet("suj");
		g.addVoeu(1, s1);
		g.addVoeu(2, s2);
		g.addVoeu(3, s3);
		g.addVoeu(4, s4);
		g.addVoeu(5, s5);
		assertTrue(g.getVoeuFini());
	}
	
	@Test
	void testGroupeAddOneVoeuAndNotReady() throws VoeuIndisponible {
		Sujet s = new Sujet("suj");
		g.addVoeu(1, s);
		assertFalse(g.getVoeuFini());
	}
}