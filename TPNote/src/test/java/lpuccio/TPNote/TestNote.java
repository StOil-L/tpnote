package lpuccio.TPNote;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestNote {
	
	Groupe g;
	Sujet s;
	
	@BeforeEach
	public void setUp() {
		g = new Groupe("Groupe des bosseurs");
		s = new Sujet("Sujet pour mettre dans son portfolio");
	}
	
	@Test
	public void testSujetDansGroupe() throws VoeuIndisponible {
		assertFalse(g.isSujetInVoeu(s));
		g.addVoeu(1, s);
		assertTrue(g.isSujetInVoeu(s));
	}
}
