package lpuccio.TPNote;

public class VoeuIndisponible extends Exception{
	
	public VoeuIndisponible(String message) {
		super(message);
	}
}
